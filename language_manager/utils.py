import base64
import os
import re
from copy import deepcopy
import logging
from datetime import datetime
from io import StringIO, SEEK_END
from subprocess import PIPE, Popen
from unittest.mock import patch
from polib import pofile, POEntry

from django.conf import settings
from django.conf import locale as dj_locales
from django.core.exceptions import ValidationError
from django.core.management import CommandError
from django.core.management.commands.makemessages import Command as DjMakeMessagesCmd
from django.template.loader import render_to_string
from django.utils.encoding import force_text, DEFAULT_LOCALE_ENCODING
from django.utils.translation import ugettext_lazy as _
from django.utils.translation.trans_real import to_locale


__all__ = (
    'get_available_base_locales',
    'validate_lang_code',
    'get_base_locales',
    'get_django_po_file',
    'get_plural_forms',
    'get_locale_dir_path',
    'generate_pot_file',
    'generate_migration',
    'generate_po_file',
    'compile_translation_from_db',
)

logger = logging.getLogger(__name__)

nplurals_re = re.compile('nplurals=(\d+)')


def get_available_base_locales():
    valid_locales = dj_locales.LANG_INFO.keys()
    base_locales = list(filter(lambda x: '-' not in x, valid_locales))  # ignore sublocales
    return base_locales


def validate_lang_code(code):
    base_locales = get_available_base_locales()
    if code not in base_locales and code.split('-')[0] not in base_locales:
        raise ValidationError(_('Wrong language code "%(code)s", it should be one of base locales %(base_locales)s '
                                'or starts from it with "-", like "en-us")' %
                                {'code': code, 'base_locales': base_locales}))
    return True


def safe_mo_load(code, _logger=None):
    # TODO: rewrite
    if _logger is None:
        _logger = logger
    from gettext import GNUTranslations

    mo_file_path = get_mo_file_path(code)
    mo_file_is_valid = False
    if os.path.isfile(mo_file_path):
        try:
            with open(mo_file_path, 'rb') as fp:
                GNUTranslations(fp=fp)
            mo_file_is_valid = True
        except:
            _logger.exception('Could not load .mo file %s' % mo_file_path)
            try:
                os.remove(mo_file_path)
            except:
                _logger.exception('Could not remove invalid .mo file %s' % mo_file_path)
    else:
        _logger.warning('Missing .mo file %s, ' % mo_file_path)

    po_file_path = get_po_file_path(code)
    if not mo_file_is_valid and os.path.isfile(po_file_path):
        logger.info('Found .po file %s, trying to rebuild .mo from it' % mo_file_path)
        pofile(po_file_path).save_as_mofile(fpath=mo_file_path)
        logger.info('New .mo file saved at path %s' % mo_file_path)
        if os.path.isfile(mo_file_path):
            try:
                with open(mo_file_path, 'rb') as fp:
                    GNUTranslations(fp=fp)
                mo_file_is_valid = True
            except:
                _logger.exception('Could not load .mo file after rebuild %s' % mo_file_path)
                try:
                    os.remove(mo_file_path)
                except:
                    _logger.exception('Could not remove rebuilded .mo file %s' % mo_file_path)
        else:
            _logger.warning('.mo file %s is missing after rebuild' % mo_file_path)
    return mo_file_is_valid


def get_base_locales(locale_or_code):
    locale = to_locale(locale_or_code)
    shortcut = locale.split('-')[0].split('_')[0].lower()
    return [locale, shortcut]


def get_po_file_path(code):
    locale = to_locale(code)
    domain = 'django'
    return os.path.join(get_locale_dir_path(), locale, 'LC_MESSAGES', '%s.po' % domain)


def get_mo_file_path(code):
    locale = to_locale(code)
    domain = 'django'
    return os.path.join(get_locale_dir_path(), locale, 'LC_MESSAGES', '%s.mo' % domain)


def get_django_po_file(locale_or_code):
    # TODO: add caching
    locales = get_base_locales(locale_or_code)
    # based on django.core.management.commands.makemessages
    import django
    for locale in locales:
        django_dir = os.path.normpath(os.path.join(os.path.dirname(django.__file__)))
        domain = 'django'
        django_po_path = os.path.join(django_dir, 'conf', 'locale', locale, 'LC_MESSAGES', '%s.po' % domain)
        if os.path.isfile(django_po_path):
            return pofile(django_po_path)
    return None


def get_plural_forms(locale_or_code):
    po_file = get_django_po_file(locale_or_code)
    if po_file:
        # TODO: is it necessary to implement case-insensitive lookup in metadata?
        return po_file.metadata.get('Plural-Forms')
    return None  # TODO: return plural-forms for english instead of None?


def get_n_plurals(locale_or_code):
    plural_forms = get_plural_forms(locale_or_code)
    match = nplurals_re.search(plural_forms or '')
    if match:
        try:
            return int(match.groups()[0])
        except ValueError:
            pass
    return 2  # Default nplurals for english


def get_locale_dir_path():
    # see django/core/management/commands/makemessages.py:348 for details
    if os.path.isdir(os.path.join('conf', 'locale')):
        return os.path.abspath(os.path.join('conf', 'locale'))
    else:
        if settings.LOCALE_PATHS:
            return settings.LOCALE_PATHS[0]
        if os.path.isdir('locale'):
            return os.path.abspath('locale')[0]
    return None


def generate_pot_file():
    message_collector = MessageCollector()
    pot_content = message_collector.get_pot_content()
    pot_file = pofile(pot_content)
    return pot_file


def generate_migration(existing_translations, ref_pot_file, app='language_manager'):
    # TODO: rewrite to use migration files instead db

    # import locally to prevent ImproperlyConfigured on start
    from language_manager.models import TranslationString
    from django.db.migrations.recorder import MigrationRecorder

    # TODO: correctly handle `previous_*`
    # no handling for "previous" in new_pot.merge(ref_pot)
    # msgmerge handles previous, but not with empty msgstr

    ts_params = []
    for entry in ref_pot_file:
        kwargs = TranslationString.from_po_entry(entry).get_kwargs()
        if any(map(lambda ts:  # is there any TranslationString
                   all(map(lambda kv: getattr(ts, kv[0], None) == kv[1], kwargs.items())),  # with all same values
                   existing_translations)):
            # skip if it is the same as in db
            continue
        params = []  # kwargs with comments
        for k, v in kwargs.items():
            if isinstance(v, bool) or isinstance(v, int) or v is None:  # values without formatting
                params.append((k, v, None))
            elif isinstance(v, str) and len(v) == 0:  # write empty string as empty string
                params.append((k, '""', None))
            else:
                # to leave value unchanged and valid for inscribing in python code
                # convert to base64, and decode during migration
                # base64.b64decode(base64.b64encode(str("any").encode('utf-8')).decode('utf-8')).decode('utf-8')
                value = base64.b64encode(str(v).encode('utf-8')).decode('utf-8')
                # make nice comment
                comments = re.sub('[^A-Za-z0-9_:/\\\!?.,\-+*\s()%\'"]+', '', str(v))
                comments = comments.split('\n')
                comments = [re.sub('\s{2,}', '', c) for c in comments]
                params.append((k, '%s(base64.b64decode("%s").decode("utf-8"))' % (type(v).__name__, value), comments))
        ts_params.append(params)

    apps = {app, 'language_manager'}
    last_migrations = list(filter(lambda x: x is not None, [
        MigrationRecorder.Migration.objects.filter(app=app).latest('id') for app in apps
    ]))
    # TODO: raise error "no initial/applied migration" for app
    if len(ts_params) > 0:
        return render_to_string('language_manager/migration.py.html', context={
            'translation_strings': ts_params,
            'required_migrations': [
                (m.app, m.name) for m in last_migrations
            ],
        })
    return None


def generate_po_file(ref_pot_file, locale_or_code, lang_translations, translation_strings):
    po_file = deepcopy(ref_pot_file)
    local_lang_names = {lt.name: lt.name_local for lt in lang_translations if lt.name_local}

    for trans in translation_strings:
        for entry in po_file:
            if entry.msgid == trans.msgid and entry.msgctxt == trans.msgctxt and not entry.is_disabled:
                if entry.msgid_plural is not None and entry.msgid_plural == trans.msgid_plural:
                    if isinstance(trans.msgstr_plural, dict):
                        # TODO: is it important to check nplurals (len())?
                        entry.msgstr_plural = trans.msgstr_plural
                else:
                    entry.msgstr = trans.msgstr
            elif entry.msgid in local_lang_names:
                entry.msgstr = local_lang_names[entry.msgid]
                del local_lang_names[entry.msgid]

    for name, name_local in local_lang_names.items():
        po_file.insert(0, POEntry(msgid=name, msgstr=name_local))

    po_file.metadata['X-Translated-Using'] = 'django-language-manager'
    po_file.metadata['PO-Revision-Date'] = datetime.now().isoformat(' ')
    plural_forms = get_plural_forms(locale_or_code)
    if plural_forms:
        po_file.metadata['Plural-Forms'] = plural_forms
    return po_file


def compile_translation_from_db(lang_codes=None, save_po=True, save_mo=True):
    from language_manager.models import LanguageDescription, TranslationString
    if lang_codes is None:
        lang_codes = LanguageDescription.objects.all()
    po_files = {}
    ref_pot_file = generate_pot_file()

    for lang in lang_codes:
        # get translations for specific language without fallbacks
        translations = TranslationString.objects.language(lang.code) \
            .filter(translations__language_code__in=[lang.code])
        lang_names = LanguageDescription.objects.language(lang.code).filter(
            translations__language_code__in=[lang.code]).all()

        po_file = generate_po_file(ref_pot_file, lang.code, lang_names, translations)
        po_files[lang.code] = po_file

        if save_po:
            po_file_path = get_po_file_path(lang.code)
            basedir = os.path.dirname(po_file_path)
            if not os.path.isdir(basedir):
                os.makedirs(basedir)
            po_file.save(fpath=po_file_path)
        if save_mo:
            mo_file_path = get_mo_file_path(lang.code)
            basedir = os.path.dirname(mo_file_path)
            if not os.path.isdir(basedir):
                os.makedirs(basedir)
            po_file.save_as_mofile(mo_file_path)

    return po_files


class NonClosableStringIO(StringIO):
    def actual_close(self, *args, **kwargs):
        super(NonClosableStringIO, self).close(*args, **kwargs)

    def close(self, *args, **kwargs):
        pass


class MessageCollector(DjMakeMessagesCmd):
    def __init__(self):
        super(MessageCollector, self).__init__()
        self.handlers = {}
        self.orig_path_exists = os.path.exists
        self.orig_open = open
        from django.core.management import utils
        self.orig_popen = utils.popen_wrapper

    def remove_potfiles(self):
        for name, handler in list(self.handlers.items()):  # changing collection during iter
            if name.endswith('.pot'):
                handler.actual_close()
                del self.handlers[name]
                # or `pass` and remove `--keep-pot` from args

    def write_po_file(self, potfile, locale):
        # super(MessageCollector, self).write_po_file()
        pass

    def close_handlers(self):
        for name, handler in self.handlers.items():
            handler.actual_close()
        self.handlers = {}

    def wrapped_pe(self, path):
        if path.endswith('.pot') or path.endswith('.po'):
            return path in self.handlers
        return self.orig_path_exists(path)

    def wrapped_open(self, file, mode='r', **kwargs):
        if file.endswith('.pot') or file.endswith('.po'):
            # pot file - po file without translation formats ...
            if file not in self.handlers:
                if mode == 'r':
                    raise FileNotFoundError()
                self.handlers[file] = NonClosableStringIO()
            if mode == 'r':
                self.handlers[file].seek(0)
            elif mode == 'w':
                self.handlers[file].actual_close()
                self.handlers[file] = NonClosableStringIO()
            elif mode == 'a':
                self.handlers[file].seek(0, SEEK_END)
            return self.handlers[file]
        return self.orig_open(file, mode, **kwargs)

    def wrapped_popen(self, args, os_err_exc_type=CommandError, stdout_encoding='utf-8'):
        # based on django.core.management.popen_wrapper
        kwargs = {  # original kwargs for Popen
            'shell': False,
            'stdout': PIPE,
            'stderr': PIPE,
            'close_fds': os.name != 'nt'
        }
        com_kwargs = {}
        if args[0] == 'msguniq':
            file = args[-1]
            if file.endswith('.pot') and file in self.handlers:
                # replace "Content-Type: text/plain; charset=CHARSET\n"
                #      to "Content-Type: text/plain; charset=UTF-8\n"
                # cause msguniq cannot determinate encoding from piped input
                file_content = self.handlers[file].getvalue() \
                    .replace("Content-Type: text/plain; charset=CHARSET\\n",
                             "Content-Type: text/plain; charset=UTF-8\\n").encode(stdout_encoding)
                kwargs['stdin'] = PIPE
                args = args[:-1]  # remove file name
                com_kwargs['input'] = file_content

        try:
            p = Popen(args, **kwargs)
        except OSError as err:
            raise os_err_exc_type('Error executing %s' % args[0]) from err
        output, errors = p.communicate(**com_kwargs)
        return (
            force_text(output, stdout_encoding, strings_only=True, errors='strict'),
            force_text(errors, DEFAULT_LOCALE_ENCODING, strings_only=True, errors='replace'),
            p.returncode
        )

    def get_pot_content(self, make_messages_args=None):
        if make_messages_args is None:
            make_messages_args = ['--keep-pot', '--ignore=env/*', '-v', '0']
        self.close_handlers()
        parser = self.create_parser('manage.py', 'make_messages')
        options = parser.parse_args(args=make_messages_args)

        with patch('os.path.exists', wraps=self.wrapped_pe):
            with patch('builtins.open', wraps=self.wrapped_open):
                with patch('django.core.management.commands.makemessages.popen_wrapper', wraps=self.wrapped_popen):
                    self.handle(**vars(options))

        po_files = list(self.handlers.values())
        if len(po_files) > 1:
            logger.warning('After executing make_messages command, files count > 1, %s' % self.handlers.keys())
        po_content = po_files[0].getvalue()
        self.close_handlers()

        return po_content
