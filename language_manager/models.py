import re

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext_lazy as _

from parler.fields import TranslatedField
from parler.models import TranslatableModel, TranslatedFields

from jsonfield import JSONField
from polib import POEntry

from language_manager.utils import get_available_base_locales, validate_lang_code

__all__ = (
    'LanguageDescription',
    'TranslationString',
    'python_format_re',
)

python_format_re = re.compile('%(?:\([^\s\)]*\))?[sdf]|\{[\w\d_]+?\}') # from rosetta source code


class LanguageDescription(TranslatableModel):
    code = models.CharField(unique=True, max_length=10, validators=[validate_lang_code],
                            help_text=_('Should be in format %(ld_format)s, like %(sample_ld)s, or just %(sample_l)s. '
                                        'Base locale should be one of %(base_locales)s') % {
                                        'ld_format': '<em>base_locale</em>-<em>dialect</em>',
                                        'sample_ld': '<em>en-us</em>',
                                        'sample_l': '<em>en</em>',
                                        'base_locales': get_available_base_locales()
                                        },
                            )
    name = models.CharField(max_length=80)
    fallbacks = models.CharField(max_length=80, help_text=_('Comma-separated list of translations to fallback'),
                                 blank=True)

    translations = TranslatedFields(
        name_local=models.CharField(max_length=80, help_text=_('Language name in user\'s language'))
    )

    def save(self, *args, **kwargs):
        # TODO: check valid base locale (code and fallbacks)
        if self.fallbacks is not None:
            self.fallbacks = re.sub(r"\s+", '', self.fallbacks)
        self.code = self.code.lower()
        super(LanguageDescription, self).save(*args, **kwargs)
        # TODO: add cache reset

    def __str__(self):
        return "%s:%s" % (self.code, self.name)


class TranslationString(TranslatableModel):
    # IMPORTANT: Add new translation strings only through migrations, it will allow better control.
    comment = models.TextField(blank=True, editable=False, help_text=_('Comment to better describe TS'))  # .
    source = models.TextField(blank=True, editable=False, help_text=_('Source references'))  #:
    flags = models.TextField(editable=False, blank=True, help_text=_('Flags (fuzzy, *-format, etc.)'))
    is_fuzzy = models.BooleanField(editable=False, default=False, help_text=_('Fuzzy state (although among flags, '
                                                                              'requires special attention, e.g.: TS was slightly changed in sources)'))
    is_obsolete = models.BooleanField(editable=False, default=False, help_text=_('TS was removed from code'))
    msgctxt = models.TextField(editable=False, null=True, blank=True, help_text=_('Disambiguating context'))
    msgid = models.TextField(editable=False, help_text=_('Original text'))
    msgid_plural = models.TextField(editable=False, blank=True)  # TODO: case when %()s in msgid_plural was changed (added new/removed one)

    is_disabled = models.BooleanField(default=False, help_text=_('If true - ignore during compiling'))

    translations = TranslatedFields(
        msgstr=models.TextField(help_text=_('Translated text'), blank=True),
        msgstr_plural=JSONField(help_text=_('Translated plurals'), blank=True)
        # store as json object with array indexes
    )

    class Meta:
        unique_together = ("msgid", "msgctxt",)

    @property
    def is_plural(self):
        return self.msgid_plural is not None and len(self.msgid_plural) > 0

    def get_python_format_validator(self, plural=False):
        # TODO: rewrite
        if plural:
            origin_list = set(python_format_re.findall(self.msgid))
        else:
            origin_list = set(python_format_re.findall(self.msgid_plural))

        def validator(value):
            new_list = set(python_format_re.findall(value))
            if origin_list != new_list:
                missing_list = origin_list - new_list
                if len(missing_list) == 0:
                    extra_list = new_list - origin_list
                    raise ValidationError(_('Extra: "%s"') % extra_list)
                raise ValidationError(_('Missing: "%s"') % missing_list)

        return validator

    def clean(self):
        if self.msgid_plural is not None and len(self.msgid_plural) > 0 and isinstance(self.msgstr_plural, dict):
            self.get_python_format_validator()(
                self.msgstr_plural.get('0', '')
            )
            plural_validator = self.get_python_format_validator(True)
            for k, v in self.msgstr_plural.items():
                if int(k) > 0:
                    plural_validator(v)
        else:
            self.get_python_format_validator()(self.msgstr)

    def save(self, *args, **kwargs):
        if self.msgctxt is not None and len(self.msgctxt) == 0:
            self.msgctxt = None
        super(TranslationString, self).save(*args, **kwargs)


    @classmethod
    def from_po_entry(cls, po_entry):
        source = '\n'.join(map(lambda x: ':'.join(x), po_entry.occurrences))
        flags = ', '.join(po_entry.flags)
        is_obsolete = bool(po_entry.obsolete)
        kwargs = dict(comment=po_entry.comment, source=source, flags=flags, is_fuzzy=po_entry.fuzzy,
                      is_obsolete=is_obsolete, msgctxt=po_entry.msgctxt, msgid=po_entry.msgid,
                      msgid_plural=po_entry.msgid_plural)
        return TranslationString(**kwargs)

    def to_po_entry(self):
        occurrences = list(map(lambda x: x.split(':'), self.source.split('\n')))
        return POEntry(
            msgid=self.msgid,
            msgid_plural=self.msgid_plural,
            msgctxt=self.msgctxt,
            obsolete=self.is_obsolete,
            comment=self.comment,
            occurrences=occurrences,
            flags=list(map(lambda x: x.strip(), self.flags.split(','))),
        )

    def get_kwargs(self):
        return dict(comment=self.comment, source=self.source, flags=self.flags, is_fuzzy=self.is_fuzzy,
                    is_obsolete=self.is_obsolete, msgctxt=self.msgctxt, msgid=self.msgid,
                    msgid_plural=self.msgid_plural)
