import logging
import gettext

import os
from datetime import datetime

from django.conf import global_settings, settings
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.utils import translation
from django.utils.translation import trans_real, get_language

from language_manager.utils import validate_lang_code, safe_mo_load

logger = logging.getLogger(__name__)

__all__ = (
    'LmSettings',
)


class LmSettings:
    def __init__(self, *args, **kwargs):
        super(LmSettings, self).__init__(*args, **kwargs)
        if 'parler' not in self.INSTALLED_APPS:
            raise ImproperlyConfigured('django-parler required fo using language_manager')

        self.PARLER_DEFAULT_LANGUAGE = self.LANGUAGE_CODE

        self.__PARLER_LANGUAGES = None
        self.__LANGUAGES = None
        self.__origin_LANG_INFO = None

        self.LM_RESETS_COUNTER = 0
        if not hasattr(self, 'TRANSLATIONS_MIGRATION_APP'):
            self.TRANSLATIONS_MIGRATION_APP = 'language_manager'

    @property
    def PARLER_LANGUAGES(self):
        if self.__PARLER_LANGUAGES is None:
            self.load_langs_info()
        return self.__PARLER_LANGUAGES
        # sample
        # return {
        #     None: (
        #         {'code': 'en'},
        #         {'code': 'ru'},
        #         {'code': 'uk',
        #           'fallbacks': ['ru', 'en'],
        #           'hide_untranslated': False
        #         },
        #     ),
        #     'default': {
        #         'fallbacks': ['en'],
        #         'hide_untranslated': True,
        #     }
        # }

    @property
    def LANGUAGES(self):
        if self.__LANGUAGES is None:
            self.load_langs_info()
        return self.__LANGUAGES
        # sample
        # return [
        #     ('en', gettext('English')),
        #     ('ru', gettext('Russian')),
        #     ('uk', gettext('Ukrainian')),
        # ]

    # @property
    # def EXTRA_LANG_INFO(self):
    #     a = 2
    #     return {
    #         'unknown': {
    #             'bidi': False,
    #             'code': 'unknown',
    #             'name': 'Unknown',
    #             'name_local': u'Unknown, fallback to "en"',
    #             'fallback': ['en'],  # one from LANGUAGES section, can there be multiple options?
    #         },
    #     }

    def load_langs_info(self):
        # self.__PARLER_LANGUAGES = None
        # self.__LANGUAGES = None

        default_lang = self.LANGUAGE_CODE
        languages = []
        parler_langs_all_sites = []

        import django.conf.locale as django_conf_locale
        if self.__origin_LANG_INFO is None:
            self.__origin_LANG_INFO = dict(django_conf_locale.LANG_INFO)

        if default_lang not in self.__origin_LANG_INFO:
            raise ImproperlyConfigured('Invalid LANGUAGE_CODE, it should be one of %s' % self.__origin_LANG_INFO.keys())

        new_lang_info = dict(self.__origin_LANG_INFO.items())

        try:
            start = datetime.now()
            from django.db import connection
            # at the moment of start parler.TranslatedModel cannot be imported, so fetch data with raw sql
            with connection.cursor() as cursor:
                # select all lang with it's local names
                cursor.execute("SELECT lmld.code, lmld.name, lmldt.name_local, lmld.fallbacks "
                               "FROM language_manager_languagedescription as lmld "
                               "LEFT JOIN language_manager_languagedescription_translation as lmldt "
                               "ON lmld.id = lmldt.master_id and lmld.code = lmldt.language_code")
                lang_info = cursor.fetchall()
            languages = []
            for code, name, name_local, fallbacks in lang_info:
                try:
                    validate_lang_code(code)
                except ValidationError as e:
                    logger.warning(e.message)
                    continue
                if not safe_mo_load(code, _logger=logger):
                    continue
                languages.append((code, name))
                # TODO: implement fallback logic
                # if fallbacks is not None and len(fallbacks) > 0:
                #     fallbacks = fallbacks.split(',')
                #     parler_langs_all_sites.append({'code': code, 'fallbacks': fallbacks})
                # else:
                parler_langs_all_sites.append({'code': code})
                new_lang_info[code]['name'] = name
                new_lang_info[code]['name_local'] = name_local if name_local is not None else name
            logger.info('Loaded languages: %s, time spent: %s' % (languages, datetime.now() - start))
        except Exception as e:
            logger.exception("Error loading languages from database, if it occurs during migration - ignore it", e)

        # add default lang, if it is absent after fetching from db
        if not any(l[0] == default_lang for l in languages):
            languages.append((default_lang, self.__origin_LANG_INFO[default_lang]))
        if not any(l['code'] == default_lang for l in parler_langs_all_sites):
            parler_langs_all_sites.append({'code': default_lang})

        # make collection like non editable
        languages = tuple(languages)
        parler_languages = {
            None: tuple(parler_langs_all_sites),
            'default': {
                'fallbacks': [default_lang],
                'hide_untranslated': False,  # default=False; let .active_translations() return fallbacks too.
            }
        }

        # patching extra langs info (why django doesn't use this as property in settings?)
        # https://stackoverflow.com/questions/12946830/how-to-add-new-languages-into-django-my-language-uyghur-or-uighur-is-not-su
        # for now, patches only names for existing languages
        django_conf_locale.LANG_INFO = new_lang_info
        self.__LANGUAGES = languages
        self.__PARLER_LANGUAGES = parler_languages

    def _reset_gettext(self):
        if not settings.USE_I18N:
            return
        # https://stackoverflow.com/questions/1392913/reloading-mo-files-for-all-processes-threads-in-django-without-a-restart
        trans_real.get_languages.cache_clear()
        try:
            # Reset gettext.GNUTranslation cache.
            gettext._translations = {}

            # Reset Django by-language translation cache.
            trans_real._translations = {}

            # Delete Django current language translation cache.
            trans_real._default = None

            # Delete translation cache for the current thread,
            # and re-activate the currently selected language (if any)
            translation.activate(get_language())
        except Exception as e:
            logger.exception("Cannot reload gettext cache", e)

    def _reset_parler(self):
        if not settings.USE_I18N:
            return

        import parler.appsettings as p
        import parler.utils.i18n as i18n

        # copy from parler.utils.i18n
        # TODO: patch ALL_LANGUAGES on start
        from django.conf.global_settings import LANGUAGES as ALL_LANGUAGES
        i18n.LANGUAGES_DICT = dict(settings.LANGUAGES)
        i18n.ALL_LANGUAGES_DICT = dict(ALL_LANGUAGES)

        # allow to override language names when  PARLER_SHOW_EXCLUDED_LANGUAGE_TABS is True:
        i18n.ALL_LANGUAGES_DICT.update(i18n.LANGUAGES_DICT)

        # copy from parler.appsettings
        from parler.utils import (
            get_parler_languages_from_django_cms,
            normalize_language_code,
        )
        from parler.utils.conf import add_default_language_settings

        p.PARLER_DEFAULT_LANGUAGE_CODE = getattr(settings, 'PARLER_DEFAULT_LANGUAGE_CODE', settings.LANGUAGE_CODE)
        p.PARLER_SHOW_EXCLUDED_LANGUAGE_TABS = getattr(settings, 'PARLER_SHOW_EXCLUDED_LANGUAGE_TABS', False)
        p.PARLER_LANGUAGES = getattr(settings, 'PARLER_LANGUAGES', {})

        if not p.PARLER_LANGUAGES:
            if hasattr(settings, 'CMS_LANGUAGES'):
                p.PARLER_LANGUAGES = get_parler_languages_from_django_cms(
                    getattr(settings, 'CMS_LANGUAGES'))

        p.PARLER_ENABLE_CACHING = getattr(settings, 'PARLER_ENABLE_CACHING', True)

        # Have to fill the default section explicitly to avoid circular imports
        p.PARLER_LANGUAGES.setdefault('default', {})
        p.PARLER_LANGUAGES['default'].setdefault('code', p.PARLER_DEFAULT_LANGUAGE_CODE)
        p.PARLER_LANGUAGES['default'].setdefault('fallbacks', [p.PARLER_DEFAULT_LANGUAGE_CODE])

        # Cleanup settings
        p.PARLER_DEFAULT_LANGUAGE_CODE = normalize_language_code(p.PARLER_DEFAULT_LANGUAGE_CODE)
        p.PARLER_LANGUAGES = add_default_language_settings(p.PARLER_LANGUAGES)

        # Activate translations by default. Flag to compensate for Django >= 1.8 default `get_language` behavior
        p.PARLER_DEFAULT_ACTIVATE = getattr(settings, 'PARLER_DEFAULT_ACTIVATE', False)

    def reset_language_cache(self):
        # TODO: add signal/trigger for resetting on multiple instances
        # reset cached value in django's lazy_settings
        settings.__dict__.__delitem__('LANGUAGES')
        settings.__dict__.__delitem__('PARLER_LANGUAGES')

        self.__PARLER_LANGUAGES = None
        self.__LANGUAGES = None
        self.LM_RESETS_COUNTER += 1

        self._reset_gettext()
        self._reset_parler()

    def __dir__(self):
        orig = super(LmSettings, self).__dir__()
        orig.append('LANGUAGES')
        orig.append('PARLER_LANGUAGES')
        return orig

    def __getattr__(self, item):
        return getattr(global_settings, item)

    def import_locals(self, full_module_name):
        reserved_props = ['LANGUAGES', 'PARLER_LANGUAGES']
        pm = __import__(full_module_name)
        for sub_module_name in full_module_name.split('.')[1:]:
            pm = getattr(pm, sub_module_name)
        for k in dir(pm):
            if not k.startswith('_') and k not in reserved_props:
                setattr(self, k, getattr(pm, k))

# settings.configure(LmSettings(), SETTINGS_MODULE=os.environ.get("DJANGO_SETTINGS_MODULE", "config.settings"))
