import os
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.migrations.executor import MigrationExecutor
from django.db import connections, DEFAULT_DB_ALIAS

import re

from language_manager.models import TranslationString
from language_manager.utils import generate_pot_file, generate_migration


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        # parser.add_argument('poll_id', nargs='+', type=int)
        pass

    def generate_migration_path(self, app):
        base_dir = os.path.dirname(__import__(app).__file__)
        migration_dir = os.path.join(base_dir, "migrations")
        try:
            last_migration_id = int(sorted(
                map(lambda x: re.search('\d+', x).group(),
                    filter(lambda x: re.match('\d+_', x),
                           os.listdir(migration_dir)))
            )[-1])
        except (IndexError, FileNotFoundError):
            return None
        return os.path.join(migration_dir, "%04i_translations.py" % (last_migration_id + 1))

    def is_database_synchronized(self):
        # https://stackoverflow.com/a/31847406/5558123
        connection = connections[DEFAULT_DB_ALIAS]
        connection.prepare_database()
        executor = MigrationExecutor(connection)
        targets = executor.loader.graph.leaf_nodes()
        return False if executor.migration_plan(targets) else True

    def handle(self, *args, **options):
        app = settings.TRANSLATIONS_MIGRATION_APP
        if not self.is_database_synchronized():
            self.stderr.write('Some migrations not applied, run "manage.py migrate" first')
        else:
            ref_pot = generate_pot_file()
            existing_translations = list(TranslationString.objects.all())
            migration = generate_migration(existing_translations, ref_pot, app)
            if migration is None:
                self.stdout.write(self.style.SUCCESS('No new translation strings, nothing to do.'))
            else:
                path = self.generate_migration_path(app)
                if path is None:
                    self.stderr.write('No initial migrations for app "%s", run "makemigrations" first' % app)
                else:
                    with open(path, 'w') as f:
                        f.writelines(migration)
                        self.stdout.write(self.style.SUCCESS('New migration at %s' % path))
