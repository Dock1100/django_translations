import logging
from datetime import datetime

from django.contrib import admin
from django.contrib import messages
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.utils import translation
from django.conf import settings
from django import forms
from django.urls import path, reverse
from django.utils.translation import ugettext_lazy as _
from parler.admin import TranslatableAdmin

from language_manager.models import LanguageDescription, TranslationString
from language_manager.utils import get_n_plurals, compile_translation_from_db, generate_pot_file, generate_migration

logger = logging.getLogger(__name__)


__all__ = (
    'BaseLmAdmin',
    'LanguageDescriptionAdmin',
    'TranslationStringAdmin',
)


class BaseLmAdmin(TranslatableAdmin):
    def __init__(self, *args, **kwargs):
        super(BaseLmAdmin, self).__init__(*args, **kwargs)

        extra_views = [self.generate_po_files, self.generate_migration, self.rebuild_and_reload]

        self.extra_urls = []  # url, view, name
        self.pretty_names = {}  # human readable names
        for view in extra_views:
            url = '%s/' % view.__name__
            name = '%s.%s' % (self.opts.label_lower, view.__name__)
            self.pretty_names[name] = _(name.split('.')[-1].replace('_', ' '))
            self.pretty_names[view.__name__] = self.pretty_names[name]
            view = self.wrap_extra_view(view)
            self.extra_urls.append(path(url, view, name=name))

    def wrap_extra_view(self, view):
        def wrapped(request, *args, **kwargs):
            response = view(request, *args, **kwargs)
            if isinstance(response, TemplateResponse):
                response.context_data.update({
                    'has_change_permission': self.has_change_permission(request),
                    'cl': {
                        'opts': self.opts,
                        'lm_action': self.pretty_names[view.__name__],
                    }
                })
            return response
        wrapped.__name__ = view.__name__
        return admin.site.admin_view(wrapped)

    def get_urls(self):
        urls = super(BaseLmAdmin, self).get_urls()
        urls = self.extra_urls + urls
        return urls

    def get_change_list_url(self):
        info = self.model._meta.app_label, self.model._meta.model_name
        return reverse('admin:%s_%s_changelist' % info)

    def changelist_view(self, request, extra_context=None):
        if extra_context is None:
            extra_context = {}
        lm_action_buttons = []
        for url_pattern in self.extra_urls:
            url = reverse('admin:%s' % url_pattern.name)
            name = self.pretty_names[url_pattern.name]
            lm_action_buttons.append((name, url))
        extra_context['lm_action_buttons'] = lm_action_buttons
        res = super(BaseLmAdmin, self).changelist_view(request, extra_context=extra_context)
        return res

    def rebuild_and_reload(self, request):
        start = datetime.now()
        try:
            compile_translation_from_db()
            settings.reset_language_cache()
        except Exception as e:
            logger.exception('Error during "rebuild_and_reload"')
            messages.error(request, _('Failed rebuilding translations, reason "%s". Check logs for details.') % e)
        else:
            messages.success(request, _('Successfully rebuilded translations. Time spent: %s') % (datetime.now() - start))
        return redirect(self.get_change_list_url())
        # return redirect('/%s/whatever/' % get_language()) TODO: check with lang in url

    def generate_po_files(self, request):
        start = datetime.now()
        po_files = compile_translation_from_db()
        po_contents = {k: v.__unicode__() for k, v in po_files.items()}
        return TemplateResponse(request, 'admin/language_manager/generate_po_files.html',
                                context={
                                    'time_spent': datetime.now() - start,
                                    'po_contents': sorted(po_contents.items()),
                                })

    def generate_migration(self, request):
        ref_pot = generate_pot_file()
        existing_translations = list(TranslationString.objects.all())
        migration = generate_migration(existing_translations, ref_pot, settings.TRANSLATIONS_MIGRATION_APP)

        return TemplateResponse(request, 'admin/language_manager/generate_migration.html',
                                context={
                                    'ref_pot_content': ref_pot.__unicode__(),
                                    'migration': migration,
                                })


class LanguageDescriptionAdmin(BaseLmAdmin):
    list_display = ('code', 'name', 'fallbacks')
    ordering = ('code',)


class TranslationStringAdmin(BaseLmAdmin):
    # TODO: rewrite

    list_display = ('msgid', 'msgctxt', 'source', 'is_disabled', 'is_fuzzy', 'is_obsolete', 'language_column')
    ordering = ('source', 'msgid', 'msgctxt')
    list_filter = ('is_disabled', 'is_fuzzy', 'is_obsolete',)

    fields = ('source', 'msgctxt', 'msgid', 'msgid_plural', 'is_fuzzy', 'is_obsolete', 'is_disabled', 'msgstr')

    def get_readonly_fields(self, request, obj=None):
        if obj is not None and obj.is_plural:
                return 'source', 'msgctxt', 'msgid', 'msgid_plural', 'is_fuzzy', 'is_obsolete'
        return 'source', 'msgctxt', 'msgid', 'is_fuzzy', 'is_obsolete'

    def get_plural_field_name(self, i):
        return 'msgstr_plural_%s' % i

    def get_idx_from_pfm(self, field_name):
        if field_name.startswith('msgstr_plural_'):
            try:
                return int(field_name.replace('msgstr_plural_', ''))
            except ValueError:
                pass
        return None

    def get_fields(self, request, obj=None):
        # TODO: implement msgstr_plural widget
        if obj is not None and obj.is_plural:
                return ['source', 'msgctxt', 'msgid', 'msgid_plural', 'is_fuzzy', 'is_obsolete', 'is_disabled'] + \
                       [self.get_plural_field_name(i) for i in range(self.n_plurals)]
        return 'source', 'msgctxt', 'msgid', 'is_fuzzy', 'is_obsolete', 'is_disabled', 'msgstr'

    def get_form(self, request, obj=None, **kwargs):
        locale = self.get_form_language(request, obj)
        if obj is not None and obj.is_plural:
            self.n_plurals = get_n_plurals(locale)
        form_class = super(TranslationStringAdmin, self).get_form(request, obj=None, **kwargs)
        if obj is not None and obj.is_plural:
            old_lang = translation.get_language()
            samples = {}
            if not locale.startswith('en'):
                translation.activate(locale)
                # TODO: find better solution to get possible plural forms
                catalog = translation._trans._active.value._catalog
                for i in range(self.n_plurals):
                    samples[i] = catalog.get(('%d day', i))
                translation.activate(old_lang)
            else:
                samples = {
                    0: '%d day',
                    1: '%d days'
                }
            for i in range(self.n_plurals):
                help_text = obj.msgid if i == 0 else obj.msgid_plural
                if samples.get(i) is not None:
                    help_text += '<br>' + _('Sample : %(local)s') % {
                        'local': '<em>%s</em>' % samples[i]
                    }
                initial = ''
                if isinstance(obj.msgstr_plural, dict):
                    initial = obj.msgstr_plural.get(str(i)) or obj.msgstr_plural.get(i) or ''
                field = forms.CharField(initial=initial, help_text=help_text, required=False,
                                        widget=forms.Textarea)
                field.widget.attrs['cols'] = '40'
                field.widget.attrs['rows'] = '2'
                form_class.base_fields[self.get_plural_field_name(i)] = field
                form_class.declared_fields[self.get_plural_field_name(i)] = field

        return form_class

    def save_form(self, request, form, change):
        unsaved_model = super(TranslationStringAdmin, self).save_form(request, form, change)
        if unsaved_model.is_plural:
            plurals = {}
            for field_name, value in form.cleaned_data.items():
                idx = self.get_idx_from_pfm(field_name)
                if idx is not None:
                    plurals[idx] = value
            unsaved_model.msgstr_plural = plurals
        return unsaved_model


admin.site.register(LanguageDescription, LanguageDescriptionAdmin)
admin.site.register(TranslationString, TranslationStringAdmin)
