from django.apps import AppConfig


class LanguageManagerConfig(AppConfig):
    name = 'language_manager'
