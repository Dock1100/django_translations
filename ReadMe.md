# About
A simple demo to change languages and translations on the fly in Django.
The project is in `prototype` state (no documentation, no tests...), use it on own risk :smile: .
Maybe, someday, it will become a module.

# Dependencies

* [django-parler](https://github.com/django-parler/django-parler) - model translations
* [polib](https://bitbucket.org/izi/polib/wiki/Home) - handling `.po` `.mo` files
* [jsonfield](https://github.com/dmkoch/django-jsonfield/) - storing JSON object in db agnostic way (used for `msgstr_plural[]`)
* `Django 2+`
* `python 3+`

# How it works

Settings `LANGUAGES` and `PARLER_LANGUAGES` are lazy loaded from database:

* Model `LanguageDescription` represents possible languages.
* Model `TranslationString` represents translations.

Actual translation strings are still stored on disk for performance and easy of use with standard `DjangoTranslations`.  
If on startup `.mo` files can not be loaded or recreated from `.po`, they will be removed.  
To apply new translations - rebuild and reload `.mo` files (`language_manager.utils.compile_translation_from_db()` and `settings.reset_language_cache())`.  

Available actions in `LanguageDescription` model admin:

* `Generate po files` - fetch translations from database and show generated content of `.po` file by languages without saving.
* `Generate migration` - collect translations and show migration for adding new `TranslationStrings` without saving.
* `Rebuild and reaload` - collect translations, create `.po` files, create `.mo` files, reset relevant caches

Highly recommended to add new `TranslationString` through migrations.


# TODO

1. Add `generate_migrations` as part of `makemigrations`
1. Change language in admin
1. Way to refresh cache/rebuild `.mo` on multiple nodes
1. Add patching for `django.conf.locale.LANG_INFO` and `django.conf.global_settings.ALL_LANGUAGES` on start (for now all codes should be based on existing locales)
1. Check `TranslatedField(any_language=True)` when some model data only in disabled language (current `en`, available`[en]`, model data in `ru`)
1. Implement fallback logic (same behavior for models and translation strings)
1. Add support for custom `Plural-Forms` (for now copies from Django)
1. For now works with only one locale path (`len(settings.LOCALE_PATHS) == 1`)
1. Add support for `djangojs` domain
1. Set default language through admin (and preconfigure fallback to en in case of error)
1. TODOs from code
1. Docs
1. Tests
1. Allow using any model-translation lib (make `django-parler` an optional dependency).
1. A lot of other stuff...
