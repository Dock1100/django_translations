from collections import OrderedDict
from datetime import datetime

from django.conf import settings
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import get_language_from_request, check_for_language, get_language_info, ngettext
from django.utils.translation.trans_real import get_supported_language_variant
from django.utils.translation import ugettext_lazy as _

from config.settings import MySettings
from demo.models import DemoData, DemoDataAnyProxy

import pprint

PrettyPrinter = pprint.PrettyPrinter(indent=4).pformat


def test_view(request):
    now = datetime.now()
    params = {}
    params.update(request.GET.dict())
    params.update(request.POST.dict())

    action = params.get('action', None)
    errors = {'lang': []}
    if action is not None:
        # use django.views.i18n.set_language instead of code bellow, it is used to store language directly in cookies
        if action == 'lang_clear':
            response = HttpResponseRedirect(request.path)
            response.delete_cookie(settings.LANGUAGE_COOKIE_NAME)
            return response
        if action == 'lang_apply':
            lang_code = params.get('code', None)
            if lang_code is None:
                errors['lang'].append('Empty code value')
            else:
                try:
                    supported = get_supported_language_variant(lang_code, strict=False)  # performs extra checks
                    lang_info = get_language_info(lang_code)
                    lang_code = supported  # lang_info['code']
                    response = HttpResponseRedirect(request.path)
                    response.set_cookie(
                        settings.LANGUAGE_COOKIE_NAME, lang_code,
                        max_age=settings.LANGUAGE_COOKIE_AGE,
                        path=settings.LANGUAGE_COOKIE_PATH,
                        domain=settings.LANGUAGE_COOKIE_DOMAIN,
                    )
                    return response
                except Exception as e:
                    errors['lang'].append('Wrong lang_code "%s"' % lang_code)
                    errors['lang'].append('%r' % e)
        # end lang switching code
        if action == 'search':
            search_text = params.get('search_text', '')
            search_type = params.get('search_type', 'all_translations')
            # TODO: add search logic
            # if len(search_text) > 0:
            #     if search_text == 'all_translations':
            #         apply_search_filter = lambda queryset: queryset.translated(translatable_field=)

    language_code = get_language_from_request(request)
    demo_data = DemoData.objects.all().order_by('id')
    any_fallback = DemoDataAnyProxy.objects.all().order_by('id')

    props_to_show = ('LANGUAGES', 'PARLER_LANGUAGES', 'PARLER_DEFAULT_LANGUAGE', 'LANGUAGE_CODE',
                     'LANGUAGE_COOKIE_NAME', 'LM_RESETS_COUNTER', )
    settings_to_show = {prop: getattr(settings, prop) for prop in props_to_show}

    plurals = list(range(1, 10)) + [11, 15, 20, 21, 25, 100]
    plurals = [(i,
                ngettext('there is %(count)d object in code',
                         'there are %(count)d objects in code', i) % {'count': i})
               for i in plurals]
    my_str = _('my string')
    return render(request, 'demo/test_template.html', context={
        'now': now,
        'language_code': language_code,
        'errors': errors,
        'my_string': my_str,
        'demo_data': demo_data,
        'demo_data_fallback_any': any_fallback,
        'demo_data_active_trans': demo_data.active_translations(),
        'settings': PrettyPrinter(settings_to_show),
        'plurals': plurals,
    })

