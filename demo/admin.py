from parler.admin import TranslatableAdmin
from django.contrib.admin import site
from .models import DemoData


class DemoDataAdmin(TranslatableAdmin):
    list_display = ('id', 'non_translatable_field', 'language_column')
    ordering = ('id',)


site.register(DemoData, DemoDataAdmin)
