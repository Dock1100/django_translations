from django.core.exceptions import ObjectDoesNotExist
from django.db import migrations, models
import base64


def create_or_update_ts(ts):
    try:
        entry = ts.__class__.objects.get(msgid=ts.msgid, msgctxt=ts.msgctxt)
        ts.pk = entry.pk
    except ObjectDoesNotExist:
        pass
    ts.save()


def forwards_func(apps, schema_editor):
    TranslationString = apps.get_model('language_manager', 'TranslationString')
    # https://github.com/django-parler/django-parler/issues/157
    TranslationString.__bases__ = (models.Model,)
    
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/admin.py:87
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9hZG1pbi5weTo4Nw==").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Failed rebuilding translations, reason "%s". Check logs for details.
        msgid=str(base64.b64decode("RmFpbGVkIHJlYnVpbGRpbmcgdHJhbnNsYXRpb25zLCByZWFzb24gIiVzIi4gQ2hlY2sgbG9ncyBmb3IgZGV0YWlscy4=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/admin.py:89
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9hZG1pbi5weTo4OQ==").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Successfully rebuilded translations. Time spent: %s
        msgid=str(base64.b64decode("U3VjY2Vzc2Z1bGx5IHJlYnVpbGRlZCB0cmFuc2xhdGlvbnMuIFRpbWUgc3BlbnQ6ICVz").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/admin.py:175
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9hZG1pbi5weToxNzU=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Sample : %(local)s
        msgid=str(base64.b64decode("U2FtcGxlIDogJShsb2NhbClz").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:27
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6Mjc=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Should be in format %(ld_format)s, like %(sample_ld)s, or just %(sample_l)s. Base locale should be one of %(base_locales)s
        msgid=str(base64.b64decode("U2hvdWxkIGJlIGluIGZvcm1hdCAlKGxkX2Zvcm1hdClzLCBsaWtlICUoc2FtcGxlX2xkKXMsIG9yIGp1c3QgJShzYW1wbGVfbClzLiBCYXNlIGxvY2FsZSBzaG91bGQgYmUgb25lIG9mICUoYmFzZV9sb2NhbGVzKXM=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:36
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6MzY=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Comma-separated list of translations to fallback
        msgid=str(base64.b64decode("Q29tbWEtc2VwYXJhdGVkIGxpc3Qgb2YgdHJhbnNsYXRpb25zIHRvIGZhbGxiYWNr").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:40
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NDA=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Language name in user's language
        msgid=str(base64.b64decode("TGFuZ3VhZ2UgbmFtZSBpbiB1c2VyJ3MgbGFuZ3VhZ2U=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:57
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NTc=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Comment to better describe TS
        msgid=str(base64.b64decode("Q29tbWVudCB0byBiZXR0ZXIgZGVzY3JpYmUgVFM=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:58
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NTg=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Source references
        msgid=str(base64.b64decode("U291cmNlIHJlZmVyZW5jZXM=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:59
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NTk=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Flags (fuzzy, *-format, etc.)
        msgid=str(base64.b64decode("RmxhZ3MgKGZ1enp5LCAqLWZvcm1hdCwgZXRjLik=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:60
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NjA=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Fuzzy state (although among flags, requires special attention, e.g.: TS was slightly changed in sources)
        msgid=str(base64.b64decode("RnV6enkgc3RhdGUgKGFsdGhvdWdoIGFtb25nIGZsYWdzLCByZXF1aXJlcyBzcGVjaWFsIGF0dGVudGlvbiwgZS5nLjogVFMgd2FzIHNsaWdodGx5IGNoYW5nZWQgaW4gc291cmNlcyk=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:62
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NjI=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # TS was removed from code
        msgid=str(base64.b64decode("VFMgd2FzIHJlbW92ZWQgZnJvbSBjb2Rl").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:63
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NjM=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Disambiguating context
        msgid=str(base64.b64decode("RGlzYW1iaWd1YXRpbmcgY29udGV4dA==").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:64
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NjQ=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Original text
        msgid=str(base64.b64decode("T3JpZ2luYWwgdGV4dA==").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:67
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6Njc=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # If true - ignore during compiling
        msgid=str(base64.b64decode("SWYgdHJ1ZSAtIGlnbm9yZSBkdXJpbmcgY29tcGlsaW5n").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:70
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NzA=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Translated text
        msgid=str(base64.b64decode("VHJhbnNsYXRlZCB0ZXh0").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:71
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NzE=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Translated plurals
        msgid=str(base64.b64decode("VHJhbnNsYXRlZCBwbHVyYWxz").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:95
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6OTU=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Extra: "%s"
        msgid=str(base64.b64decode("RXh0cmE6ICIlcyI=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:96
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6OTY=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Missing: "%s"
        msgid=str(base64.b64decode("TWlzc2luZzogIiVzIg==").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/templates/admin/language_manager/change_list.html:7
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci90ZW1wbGF0ZXMvYWRtaW4vbGFuZ3VhZ2VfbWFuYWdlci9jaGFuZ2VfbGlzdC5odG1sOjc=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Home
        msgid=str(base64.b64decode("SG9tZQ==").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/templates/admin/language_manager/change_list.html:30
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci90ZW1wbGF0ZXMvYWRtaW4vbGFuZ3VhZ2VfbWFuYWdlci9jaGFuZ2VfbGlzdC5odG1sOjMw").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Add %(name)s
        msgid=str(base64.b64decode("QWRkICUobmFtZSlz").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/utils.py:50
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci91dGlscy5weTo1MA==").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Wrong language code "%(code)s", it should be one of base locales %(base_locales)s or starts from it with "-", like "en-us")
        msgid=str(base64.b64decode("V3JvbmcgbGFuZ3VhZ2UgY29kZSAiJShjb2RlKXMiLCBpdCBzaG91bGQgYmUgb25lIG9mIGJhc2UgbG9jYWxlcyAlKGJhc2VfbG9jYWxlcylzIG9yIHN0YXJ0cyBmcm9tIGl0IHdpdGggIi0iLCBsaWtlICJlbi11cyIp").decode("utf-8")),
        msgid_plural="",
    ))


class Migration(migrations.Migration):

    dependencies = [
        
        ('language_manager', '0002_add_def_lang'),
        ('demo', '0007_translations'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]
