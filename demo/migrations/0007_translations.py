from django.core.exceptions import ObjectDoesNotExist
from django.db import migrations, models
import base64


def create_or_update_ts(ts):
    try:
        entry = ts.__class__.objects.get(msgid=ts.msgid, msgctxt=ts.msgctxt)
        ts.pk = entry.pk
    except ObjectDoesNotExist:
        pass
    ts.save()


def forwards_func(apps, schema_editor):
    TranslationString = apps.get_model('language_manager', 'TranslationString')
    # https://github.com/django-parler/django-parler/issues/157
    TranslationString.__bases__ = (models.Model,)

    create_or_update_ts(TranslationString(
        comment="",
        # demo/templates/demo/test_template.html:66
        source=str(base64.b64decode("ZGVtby90ZW1wbGF0ZXMvZGVtby90ZXN0X3RlbXBsYXRlLmh0bWw6NjY=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        # context_1
        msgctxt=str(base64.b64decode("Y29udGV4dF8x").decode("utf-8")),
        # Hi with different context2!
        msgid=str(base64.b64decode("SGkgd2l0aCBkaWZmZXJlbnQgY29udGV4dDIh").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # demo/templates/demo/test_template.html:67
        source=str(base64.b64decode("ZGVtby90ZW1wbGF0ZXMvZGVtby90ZXN0X3RlbXBsYXRlLmh0bWw6Njc=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        # context_2
        msgctxt=str(base64.b64decode("Y29udGV4dF8y").decode("utf-8")),
        # Hi with different context2!
        msgid=str(base64.b64decode("SGkgd2l0aCBkaWZmZXJlbnQgY29udGV4dDIh").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # demo/templates/demo/test_template.html:79
        source=str(base64.b64decode("ZGVtby90ZW1wbGF0ZXMvZGVtby90ZXN0X3RlbXBsYXRlLmh0bWw6Nzk=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # once in tamplate 
        msgid=str(base64.b64decode("b25jZSBpbiB0YW1wbGF0ZSA=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # demo/templates/demo/test_template.html:81
        source=str(base64.b64decode("ZGVtby90ZW1wbGF0ZXMvZGVtby90ZXN0X3RlbXBsYXRlLmh0bWw6ODE=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # 
        # %(plural_counter)s time in template 
        msgid=str(base64.b64decode("CiAgICAgICAgICAgICAgICAlKHBsdXJhbF9jb3VudGVyKXMgdGltZSBpbiB0ZW1wbGF0ZSA=").decode(
            "utf-8")),
        #  %(plural_counter)s times in template
        # 
        msgid_plural=str(
            base64.b64decode("ICUocGx1cmFsX2NvdW50ZXIpcyB0aW1lcyBpbiB0ZW1wbGF0ZQogICAgICAgICAgICA=").decode("utf-8")),
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # demo/templates/demo/test_template.html:94
        source=str(base64.b64decode("ZGVtby90ZW1wbGF0ZXMvZGVtby90ZXN0X3RlbXBsYXRlLmh0bWw6OTQ=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # This string will be translated, current date is %(now)s.
        msgid=str(
            base64.b64decode("VGhpcyBzdHJpbmcgd2lsbCBiZSB0cmFuc2xhdGVkLCBjdXJyZW50IGRhdGUgaXMgJShub3cpcy4=").decode(
                "utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # demo/views.py:73
        source=str(base64.b64decode("ZGVtby92aWV3cy5weTo3Mw==").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # there is %(count)d object in code
        msgid=str(base64.b64decode("dGhlcmUgaXMgJShjb3VudClkIG9iamVjdCBpbiBjb2Rl").decode("utf-8")),
        # there are %(count)d objects in code
        msgid_plural=str(base64.b64decode("dGhlcmUgYXJlICUoY291bnQpZCBvYmplY3RzIGluIGNvZGU=").decode("utf-8")),
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # demo/views.py:76
        source=str(base64.b64decode("ZGVtby92aWV3cy5weTo3Ng==").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # my string
        msgid=str(base64.b64decode("bXkgc3RyaW5n").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/admin.py:101
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9hZG1pbi5weToxMDE=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Sample : %(local)s
        msgid=str(base64.b64decode("U2FtcGxlIDogJShsb2NhbClz").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:26
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6MjY=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Should be in format %(ld_format)s, like %(sample_ld)s, or just %(sample_l)s. Base locale should be one of %(base_locales)s
        msgid=str(base64.b64decode(
            "U2hvdWxkIGJlIGluIGZvcm1hdCAlKGxkX2Zvcm1hdClzLCBsaWtlICUoc2FtcGxlX2xkKXMsIG9yIGp1c3QgJShzYW1wbGVfbClzLiBCYXNlIGxvY2FsZSBzaG91bGQgYmUgb25lIG9mICUoYmFzZV9sb2NhbGVzKXM=").decode(
            "utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:35
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6MzU=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Comma-separated list of translations to fallback
        msgid=str(base64.b64decode("Q29tbWEtc2VwYXJhdGVkIGxpc3Qgb2YgdHJhbnNsYXRpb25zIHRvIGZhbGxiYWNr").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:39
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6Mzk=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Language name in users language
        msgid=str(base64.b64decode("TGFuZ3VhZ2UgbmFtZSBpbiB1c2VyJ3MgbGFuZ3VhZ2U=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:55
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NTU=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Comment to better describe TS
        msgid=str(base64.b64decode("Q29tbWVudCB0byBiZXR0ZXIgZGVzY3JpYmUgVFM=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:56
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NTY=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Source references
        msgid=str(base64.b64decode("U291cmNlIHJlZmVyZW5jZXM=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:57
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NTc=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Flags (fuzzy, *-format, etc.)
        msgid=str(base64.b64decode("RmxhZ3MgKGZ1enp5LCAqLWZvcm1hdCwgZXRjLik=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:58
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NTg=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Fuzzy state (although among flags, requires special attention, e.g.: TS was slightly changed in sources)
        msgid=str(base64.b64decode(
            "RnV6enkgc3RhdGUgKGFsdGhvdWdoIGFtb25nIGZsYWdzLCByZXF1aXJlcyBzcGVjaWFsIGF0dGVudGlvbiwgZS5nLjogVFMgd2FzIHNsaWdodGx5IGNoYW5nZWQgaW4gc291cmNlcyk=").decode(
            "utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:60
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NjA=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # TS was removed from code
        msgid=str(base64.b64decode("VFMgd2FzIHJlbW92ZWQgZnJvbSBjb2Rl").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:61
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NjE=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Disambiguating context
        msgid=str(base64.b64decode("RGlzYW1iaWd1YXRpbmcgY29udGV4dA==").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:62
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NjI=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Original text
        msgid=str(base64.b64decode("T3JpZ2luYWwgdGV4dA==").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:65
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6NjU=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # If true - ignore during compiling
        msgid=str(base64.b64decode("SWYgdHJ1ZSAtIGlnbm9yZSBkdXJpbmcgY29tcGlsaW5n").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:68
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6Njg=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Translated text
        msgid=str(base64.b64decode("VHJhbnNsYXRlZCB0ZXh0").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:69
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6Njk=").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Translated plurals
        msgid=str(base64.b64decode("VHJhbnNsYXRlZCBwbHVyYWxz").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:92
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6OTI=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Extra: %s
        msgid=str(base64.b64decode("RXh0cmE6ICIlcyI=").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/models.py:93
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci9tb2RlbHMucHk6OTM=").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Missing: %s
        msgid=str(base64.b64decode("TWlzc2luZzogIiVzIg==").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/templates/admin/index.html:13
        source=str(
            base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci90ZW1wbGF0ZXMvYWRtaW4vaW5kZXguaHRtbDoxMw==").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Models in the %(name)s application
        msgid=str(base64.b64decode("TW9kZWxzIGluIHRoZSAlKG5hbWUpcyBhcHBsaWNhdGlvbg==").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/templates/admin/index.html:24
        source=str(
            base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci90ZW1wbGF0ZXMvYWRtaW4vaW5kZXguaHRtbDoyNA==").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Add
        msgid=str(base64.b64decode("QWRk").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/templates/admin/index.html:30
        source=str(
            base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci90ZW1wbGF0ZXMvYWRtaW4vaW5kZXguaHRtbDozMA==").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Change
        msgid=str(base64.b64decode("Q2hhbmdl").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/templates/admin/index.html:59
        source=str(
            base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci90ZW1wbGF0ZXMvYWRtaW4vaW5kZXguaHRtbDo1OQ==").decode("utf-8")),
        flags="",
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # You dont have permission to edit anything.
        msgid=str(base64.b64decode("WW91IGRvbid0IGhhdmUgcGVybWlzc2lvbiB0byBlZGl0IGFueXRoaW5nLg==").decode("utf-8")),
        msgid_plural="",
    ))
    create_or_update_ts(TranslationString(
        comment="",
        # language_manager/utils.py:40
        source=str(base64.b64decode("bGFuZ3VhZ2VfbWFuYWdlci91dGlscy5weTo0MA==").decode("utf-8")),
        # python-format
        flags=str(base64.b64decode("cHl0aG9uLWZvcm1hdA==").decode("utf-8")),
        is_fuzzy=False,
        is_obsolete=False,
        msgctxt=None,
        # Wrong language code %(code)s, it should be one of base locales %(base_locales)s or starts from it with -, like en-us)
        msgid=str(base64.b64decode(
            "V3JvbmcgbGFuZ3VhZ2UgY29kZSAiJShjb2RlKXMiLCBpdCBzaG91bGQgYmUgb25lIG9mIGJhc2UgbG9jYWxlcyAlKGJhc2VfbG9jYWxlcylzIG9yIHN0YXJ0cyBmcm9tIGl0IHdpdGggIi0iLCBsaWtlICJlbi11cyIp").decode(
            "utf-8")),
        msgid_plural="",
    ))


class Migration(migrations.Migration):
    dependencies = [
        ('demo', '0006_demodataanyproxy'),
        ('language_manager', '0002_add_def_lang'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]