from __future__ import unicode_literals

from django.db import models
from parler.fields import TranslatedField
from parler.models import TranslatableModel, TranslatedFields


class DemoData(TranslatableModel):
    id = models.AutoField(primary_key=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    non_translatable_field = models.CharField(max_length=80)
    # translatable_field = TranslatedField(any_language=True)

    translations = TranslatedFields(
        translatable_field=models.CharField(max_length=80),
    )

    def __str__(self):
        return "Demo data id:%s" % self.id


class DemoDataAnyProxy(DemoData):
    translatable_field = TranslatedField(any_language=True)

    class Meta:
        proxy = True